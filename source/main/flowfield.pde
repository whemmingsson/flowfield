class Flowfield {
  private PVector[][] _vectors;
  private int _rows;
  private int _columns;
  
  private float _timeOffset;
  
  Flowfield(int rows, int columns){
    _timeOffset = 0;
    _rows = rows;
    _columns = columns;
    _vectors = new PVector[rows][columns];
    create();
  }
  
  int getRows(){
    return _rows;
  }
  
  int getColumns(){  
    return _columns;
  }
  
   PVector[][] getVectors() {
     return _vectors;
   }
   
   PVector getVectorAt(int x, int y) {
     return _vectors[y][x];
   }
   
        
  void create() {
    float yOffset = 0;
    for(int y = 0; y < _rows; y++){
      float xOffset = 0;
      for(int x = 0; x < _columns; x++) {
        xOffset += Settings.Field.OFFSET_SPEED;
        float noise = noise(xOffset, yOffset, _timeOffset);
        setVectorAt(createForceVector(noise),x,y);       
      }
    yOffset += Settings.Field.OFFSET_SPEED;
    }
      
     if(Settings.Field.ENABLE_TIME) {
      _timeOffset += Settings.Field.TIME_OFFSET_SPEED;
    }
  }
  
  private PVector createForceVector(float noise){  
      float angle = noise * TWO_PI;
      PVector v = PVector.fromAngle(angle);   
      v.setMag(Settings.Field.FORCE_STRENGTH);
      return v;
  }
  
  private void setVectorAt(PVector v, int x, int y){
    _vectors[y][x] = v;
  } 
}