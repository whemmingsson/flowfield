static class Settings {
 
  static class Field {
     static final float SCALE = 10;
     static final boolean ENABLE_TIME = true;
     static final float OFFSET_SPEED = 0.08;
     static final float FORCE_STRENGTH = 0.6;
     static final float TIME_OFFSET_SPEED = 0.008;
  }
  
  static class Particle {
    static final int MAX_SPEED = 2;
    static final float THICKNESS = 1;
    static final int ALPHA = 5;
  }
  
  static class Environment {
    static final int NUMBER_OF_PARTICLES = 8000;
    static final boolean USE_RAINBOW = true; 
    static final boolean DRAW_VECTORS = false;
    static final boolean DRAW_PARTICLES = true;
    static final boolean DRAW_FPS = true;
  }
    
}