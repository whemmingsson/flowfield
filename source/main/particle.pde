class Particle {  
  private PVector _position;
  private PVector _prevPosition;
  private PVector _velocity;
  private PVector _acceleration;
  
  private int _hue;
  
  Particle(){
   _position = new PVector(0,0); 
   _prevPosition = new PVector(0,0); 
   _velocity = new PVector(0,0); 
   _acceleration = new PVector(0,0);
   _hue = 0;
  }
  
   Particle(PVector startingPosition){
   _position = startingPosition;
   _prevPosition = startingPosition.copy();
   _velocity = new PVector(0,0); 
   _acceleration = new PVector(0,0);  
   _hue = 0;
  }
  
  void update() {
    _velocity.add(_acceleration);
    _velocity.limit(Settings.Particle.MAX_SPEED);
    _position.add(_velocity);
    _acceleration.mult(0);   
  }
  
  void applyForce(PVector force) {
   _acceleration.add(force); 
  }
  
  void follow(PVector[][] flowField){
    int x = floor(_position.x / Settings.Field.SCALE);
    int y = floor(_position.y / Settings.Field.SCALE);      
    applyForce(flowField[y][x]); //<>//
  }    
  
  void stopAtEdges(){
    if(_position.x >= width){
      _position.x = 1;
      updatePrev();
    }
    if(_position.x <= 0){
      _position.x = width -1;
      updatePrev();
    }
     if(_position.y >= height){
      _position.y = 1;
      updatePrev();
    }
     if(_position.y <= 0){
      _position.y = height -1;
      updatePrev();
    }
  }
  
  void updatePrev() {
    _prevPosition.x = _position.x;
    _prevPosition.y = _position.y;   
  }
  
  void render(){
    _hue += 1;
    
    if(_hue == 255){
      _hue = 0;
    }
    
    if(Settings.Environment.USE_RAINBOW) {
      stroke(_hue, 255, 255, Settings.Particle.ALPHA);
    }
    else {
      stroke(255, Settings.Particle.ALPHA);
    }
    strokeWeight(Settings.Particle.THICKNESS);
    
   line(_prevPosition.x,_prevPosition.y,_position.x, _position.y);
    
    
    updatePrev();       
  }  
       
}