Particle[] _particles;
Flowfield _field;

// SETUP
void setup() {
  size(1400,800, FX2D);
  background(20);
  noiseDetail(15);
  strokeCap(SQUARE);
 
  if(Settings.Environment.USE_RAINBOW){
    colorMode(HSB,255);
  }
    
  _particles = new Particle[Settings.Environment.NUMBER_OF_PARTICLES];
  initParticles(); 
 
  _field = new Flowfield(floor(height / Settings.Field.SCALE),floor(width / Settings.Field.SCALE));  
  
  if(Settings.Environment.DRAW_VECTORS) { //<>//
    drawVectorsInField();
  }
}

void initParticles(){
  for(int i = 0; i< Settings.Environment.NUMBER_OF_PARTICLES; i++){
    _particles[i] = new Particle(new PVector(random(width), random(height)));
  }
}

// DRAW
void draw() { 
  if(Settings.Environment.DRAW_VECTORS) {
    background(20);
  }
  
  if(Settings.Field.ENABLE_TIME){
    _field.create();
    
  if(Settings.Environment.DRAW_VECTORS){
      drawVectorsInField();
    }
  }
  
  if(Settings.Environment.DRAW_PARTICLES){
    updateAndDrawParticles(); 
  }
    
  if(Settings.Environment.DRAW_FPS){
   drawFps();
  }
}

void drawVectorsInField(){
  strokeWeight(1);
  for(int y = 0; y < _field.getRows(); y++){
    for(int x = 0; x < _field.getColumns(); x++) {
      drawVector(x, y);
    }
  }
}

void drawVector(int x, int y) {
    pushMatrix();
    stroke(40);
    translate(x*Settings.Field.SCALE, y*Settings.Field.SCALE);
    rotate(_field.getVectorAt(x,y).heading());
    line(0,0, Settings.Field.SCALE, 0);    
    popMatrix();
}

void updateAndDrawParticles(){
  for(int i = 0; i< Settings.Environment.NUMBER_OF_PARTICLES; i++){
    _particles[i].follow(_field.getVectors());
    _particles[i].update();
    _particles[i].stopAtEdges();
    _particles[i].render();
  }
}

void drawFps(){
    fill(0);
    noStroke();
    rect(7,00, 60, 30);
    fill(255);
    text("fps :"+floor(frameRate), 20, 20);
    noFill();
}