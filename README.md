# FLOWFIELD USING PERLIN NOISE #

This Processing application uses Perlin Noise to create a flowfield, where particles can move around according to the fields vectors.

![Example](https://bytebucket.org/whemmingsson/flowfield/raw/3486690bca441d642dd86472360a184ef46186a4/resources/example.png)